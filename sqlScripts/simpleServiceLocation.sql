SELECT serviceLocation.siteID, serviceLocation.customerID, customerData.companyName, serviceLocation.siteDescription, serviceLocation.sitePhysicalAddress, physicalAddress.houseNumber, physicalAddress.streetName, physicalAddress.cityName, physicalAddress.stateAbbreviation, 
             physicalAddress.zipcode, serviceLocation.siteContactName, people.firstName + ' ' + people.lastName AS POCname, phoneNumber.phoneDescription, phoneNumber.phoneNumber, emailAddress.emailDescription, emailAddress.emailAddress, 
             serviceLocation.siteArrivalInstructions
FROM   serviceLocation INNER JOIN
             physicalAddress ON serviceLocation.sitePhysicalAddress = physicalAddress.mailID INNER JOIN
             people ON serviceLocation.siteContactName = people.personID INNER JOIN
             customerData ON serviceLocation.customerID = customerData.customerID INNER JOIN
             phoneNumber ON people.personID = phoneNumber.personID INNER JOIN
             emailAddress ON people.personID = emailAddress.ownerID
WHERE (phoneNumber.phoneDescription = 'Office') AND (emailAddress.emailDescription = 'Work')