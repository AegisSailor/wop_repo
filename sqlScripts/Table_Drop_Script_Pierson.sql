/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Student: Roger Pierson
Purpose: Drop and re-creae Work Order Processing Database
Rev: 2021-03-28
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
BEGIN TRANSACTION
SET XACT_ABORT ON -- Abort entire transaction on any error

IF DB_ID (N'workOrderSystem') IS NOT NULL
	DROP DATABASE workOrderSystem;
	GO

CREATE DATABASE workOrderSystem;
GO

COMMIT;