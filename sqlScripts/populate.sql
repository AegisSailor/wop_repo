/****** Object:  Table [dbo].[customerData]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

/* CREATE USER AND ROLES */
CREATE USER limitedUser	FOR LOGIN lowUser
CREATE ROLE [noContactInfo]
ALTER ROLE [noContactInfo] ADD MEMBER [limitedUser]; 

/* CREATE TABLES */
CREATE TABLE [dbo].[customerData](
	[customerID] [tinyint] IDENTITY(1,1) NOT NULL,
	[companyName] [varchar](255) NULL,
	[hqPhysicalAddress] [tinyint] NULL,
	[hqContactName] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[customerID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[emailAddress]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[emailAddress](
	[emailID] [tinyint] IDENTITY(1,1) NOT NULL,
	[ownerID] [tinyint] NULL,
	[emailDescription] [varchar](255) NULL,
	[emailAddress] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[emailID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[fieldTechData]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[fieldTechData](
	[TechID] [tinyint] IDENTITY(1,1) NOT NULL,
	[TechName] [tinyint] NULL,
	[laborRate] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[TechID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[people]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[people](
	[personID] [tinyint] IDENTITY(1,1) NOT NULL,
	[personCategory] [varchar](255) NULL,
	[firstName] [varchar](255) NULL,
	[lastName] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[personID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[phoneNumber]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[phoneNumber](
	[phoneID] [tinyint] IDENTITY(1,1) NOT NULL,
	[personID] [tinyint] NULL,
	[phoneDescription] [varchar](255) NULL,
	[phoneNumber] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[phoneID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[physicalAddress]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[physicalAddress](
	[mailID] [tinyint] IDENTITY(1,1) NOT NULL,
	[locationType] [varchar](255) NULL,
	[houseNumber] [varchar](255) NULL,
	[streetName] [varchar](255) NULL,
	[cityName] [varchar](255) NULL,
	[stateAbbreviation] [varchar](255) NULL,
	[zipcode] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[mailID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[serviceLocation]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[serviceLocation](
	[siteID] [tinyint] IDENTITY(1,1) NOT NULL,
	[siteDescription] [varchar](255) NULL,
	[customerID] [tinyint] NULL,
	[sitePhysicalAddress] [tinyint] NULL,
	[siteContactName] [tinyint] NULL,
	[siteArrivalInstructions] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[siteID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[workOrder]    Script Date: 4/4/2021 9:05:59 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[workOrder](
	[workOrderNumber] [tinyint] IDENTITY(1,1) NOT NULL,
	[siteID] [tinyint] NULL,
	[techID] [tinyint] NULL,
	[customerComplaint] [varchar](255) NULL,
	[estimatedHours] [varchar](255) NULL,
	[estimatedCost] [varchar](255) NULL,
	[techDescriptionOfWork] [varchar](255) NULL,
	[jobStartTime] [varchar](255) NULL,
	[jobStopTime] [varchar](255) NULL,
	[actualCost] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[workOrderNumber] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

SET IDENTITY_INSERT [dbo].[customerData] ON 

INSERT [dbo].[customerData] ([customerID], [companyName], [hqPhysicalAddress], [hqContactName]) VALUES (1, N'Tool Time', 1, 1)

INSERT [dbo].[customerData] ([customerID], [companyName], [hqPhysicalAddress], [hqContactName]) VALUES (2, N'Fence Co.', 4, 4)

SET IDENTITY_INSERT [dbo].[customerData] OFF

SET IDENTITY_INSERT [dbo].[emailAddress] ON 

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (1, 1, N'Work', N'tim.taylor@work.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (2, 1, N'Home', N'tim.taylor@home.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (3, 2, N'Work', N'al.borland@work.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (4, 2, N'Home', N'al.borland@home.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (5, 3, N'Work', N'jill.taylor@work.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (6, 3, N'Home', N'jill.taylor@home.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (7, 4, N'Work', N'wilson.wilson@work.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (8, 4, N'Home', N'wilson.wilson@home.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (9, 5, N'Work', N'tina.fey@work.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (10, 6, N'Work', N'amy.pohler@work.com')

INSERT [dbo].[emailAddress] ([emailID], [ownerID], [emailDescription], [emailAddress]) VALUES (11, 7, N'Work', N'jimmy.fallon@work.com')

SET IDENTITY_INSERT [dbo].[emailAddress] OFF

SET IDENTITY_INSERT [dbo].[fieldTechData] ON 

INSERT [dbo].[fieldTechData] ([TechID], [TechName], [laborRate]) VALUES (1, 5, N'30')

INSERT [dbo].[fieldTechData] ([TechID], [TechName], [laborRate]) VALUES (2, 6, N'35')

INSERT [dbo].[fieldTechData] ([TechID], [TechName], [laborRate]) VALUES (3, 7, N'40')

SET IDENTITY_INSERT [dbo].[fieldTechData] OFF

SET IDENTITY_INSERT [dbo].[people] ON 

INSERT [dbo].[people] ([personID], [personCategory], [firstName], [lastName]) VALUES (1, N'Customer', N'Tim', N'Taylor')

INSERT [dbo].[people] ([personID], [personCategory], [firstName], [lastName]) VALUES (2, N'Customer', N'Al', N'Borland')

INSERT [dbo].[people] ([personID], [personCategory], [firstName], [lastName]) VALUES (3, N'Customer', N'Jill', N'Taylor')

INSERT [dbo].[people] ([personID], [personCategory], [firstName], [lastName]) VALUES (4, N'Customer', N'Wilson', N'Wilson')

INSERT [dbo].[people] ([personID], [personCategory], [firstName], [lastName]) VALUES (5, N'Technician', N'Tina', N'Fey')

INSERT [dbo].[people] ([personID], [personCategory], [firstName], [lastName]) VALUES (6, N'Technician', N'Amy', N'Pohler')

INSERT [dbo].[people] ([personID], [personCategory], [firstName], [lastName]) VALUES (7, N'Technician', N'Jimmy', N'Fallon')

SET IDENTITY_INSERT [dbo].[people] OFF

SET IDENTITY_INSERT [dbo].[phoneNumber] ON 

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (1, 1, N'Office', N'555-1111')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (2, 1, N'Cell', N'444-1111')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (3, 2, N'Offfice', N'555-2222')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (4, 2, N'Cell', N'444-2222')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (5, 3, N'Office', N'555-3333')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (6, 3, N'Cell', N'444-3333')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (7, 4, N'Office', N'555-4444')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (8, 4, N'Cell', N'444-4444')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (9, 5, N'Office', N'555-5555')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (10, 5, N'Cell', N'444-5555')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (11, 6, N'Office', N'555-6666')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (12, 6, N'Cell', N'444-6666')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (13, 7, N'Office', N'555-7777')

INSERT [dbo].[phoneNumber] ([phoneID], [personID], [phoneDescription], [phoneNumber]) VALUES (14, NULL, N'Cell', N'444-7777')

SET IDENTITY_INSERT [dbo].[phoneNumber] OFF

SET IDENTITY_INSERT [dbo].[physicalAddress] ON 

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (1, N'Headquarters', N'111', N'HQ St.', N'Truth or Consequences', N'NM', N'11111')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (2, N'Site', N'112', N'Site St.', N'Santa Fe', N'NM', N'11112')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (3, N'Site', N'113', N'Site St.', N'Las Cruces', N'NM', N'11113')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (4, N'Headquarters', N'221', N'HQ St.', N'Grapevine', N'TX', N'22221')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (5, N'Site', N'222', N'Site St.', N'Irving', N'TX', N'22222')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (6, N'Site', N'223', N'Site St.', N'Fort Worth', N'TX', N'22223')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (7, N'Headquarters', N'331', N'HQ St.', N'St. Augistine', N'FL', N'33331')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (8, N'Site', N'332', N'Site St. ', N'Daytona Beach', N'FL', N'33332')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (9, N'Headquarters', N'441', N'HQ St. ', N'Lima', N'OH', N'44441')

INSERT [dbo].[physicalAddress] ([mailID], [locationType], [houseNumber], [streetName], [cityName], [stateAbbreviation], [zipcode]) VALUES (10, N'Site', N'441', N'Site St.', N'Kettering', N'OH', N'44442')

SET IDENTITY_INSERT [dbo].[physicalAddress] OFF

SET IDENTITY_INSERT [dbo].[serviceLocation] ON 

INSERT [dbo].[serviceLocation] ([siteID], [siteDescription], [customerID], [sitePhysicalAddress], [siteContactName], [siteArrivalInstructions]) VALUES (1, N'Binford', 1, 1, 2, N'Ask if everyone knows what time it is')

INSERT [dbo].[serviceLocation] ([siteID], [siteDescription], [customerID], [sitePhysicalAddress], [siteContactName], [siteArrivalInstructions]) VALUES (2, N'Back yard', 2, 4, 3, N'Ask a deep question')

SET IDENTITY_INSERT [dbo].[serviceLocation] OFF

SET IDENTITY_INSERT [dbo].[workOrder] ON 

INSERT [dbo].[workOrder] ([workOrderNumber], [siteID], [techID], [customerComplaint], [estimatedHours], [estimatedCost], [techDescriptionOfWork], [jobStartTime], [jobStopTime], [actualCost]) VALUES (1, 1, 1, N'Malfunctioning nail gun', N'3', N'400.00', N'Removed "expert" modificaitons', N'15:00', N'18:00', N'324.00')

INSERT [dbo].[workOrder] ([workOrderNumber], [siteID], [techID], [customerComplaint], [estimatedHours], [estimatedCost], [techDescriptionOfWork], [jobStartTime], [jobStopTime], [actualCost]) VALUES (2, 1, 2, N'Malfunctioning lawn mower', N'2', N'200', N'Removed jet engine', N'11:00', N'13:00', N'275.00')

INSERT [dbo].[workOrder] ([workOrderNumber], [siteID], [techID], [customerComplaint], [estimatedHours], [estimatedCost], [techDescriptionOfWork], [jobStartTime], [jobStopTime], [actualCost]) VALUES (3, 2, 3, N'Fence too short', N'4', N'530.00', N'Dug trench to stand in on one side of fence.', N'0900', N'12:00', N'425.00')

INSERT [dbo].[workOrder] ([workOrderNumber], [siteID], [techID], [customerComplaint], [estimatedHours], [estimatedCost], [techDescriptionOfWork], [jobStartTime], [jobStopTime], [actualCost]) VALUES (4, 2, 1, N'Fire damage from neighbors grill', N'2', N'310', N'Replaced damage boards', N'1400', N'15:30', N'280.00')

SET IDENTITY_INSERT [dbo].[workOrder] OFF

ALTER TABLE [dbo].[customerData]  WITH CHECK ADD FOREIGN KEY([hqContactName])
REFERENCES [dbo].[people] ([personID])

ALTER TABLE [dbo].[customerData]  WITH CHECK ADD FOREIGN KEY([hqPhysicalAddress])
REFERENCES [dbo].[physicalAddress] ([mailID])

ALTER TABLE [dbo].[emailAddress]  WITH CHECK ADD FOREIGN KEY([ownerID])
REFERENCES [dbo].[people] ([personID])

ALTER TABLE [dbo].[fieldTechData]  WITH CHECK ADD FOREIGN KEY([TechName])
REFERENCES [dbo].[people] ([personID])

ALTER TABLE [dbo].[phoneNumber]  WITH CHECK ADD FOREIGN KEY([personID])
REFERENCES [dbo].[people] ([personID])

ALTER TABLE [dbo].[serviceLocation]  WITH CHECK ADD FOREIGN KEY([customerID])
REFERENCES [dbo].[customerData] ([customerID])

ALTER TABLE [dbo].[serviceLocation]  WITH CHECK ADD FOREIGN KEY([siteContactName])
REFERENCES [dbo].[people] ([personID])

ALTER TABLE [dbo].[serviceLocation]  WITH CHECK ADD FOREIGN KEY([sitePhysicalAddress])
REFERENCES [dbo].[physicalAddress] ([mailID])

ALTER TABLE [dbo].[workOrder]  WITH CHECK ADD FOREIGN KEY([siteID])
REFERENCES [dbo].[serviceLocation] ([siteID])

ALTER TABLE [dbo].[workOrder]  WITH CHECK ADD FOREIGN KEY([techID])
REFERENCES [dbo].[fieldTechData] ([TechID])

ALTER DATABASE [wopDB] SET  READ_WRITE 

/*SET PERMISSIONS */
DENY SELECT ON [dbo].[phoneNumber] TO [noContactInfo]
DENY SELECT ON [dbo].[emailAddress] TO [noContactInfo]

GRANT ALTER ON [dbo].[people] TO [noContactInfo]
GRANT INSERT ON [dbo].[people] TO [noContactInfo]
GRANT SELECT ON [dbo].[people] TO [noContactInfo]

GRANT ALTER ON [dbo].[customerData] TO [noContactInfo]
GRANT INSERT ON [dbo].[customerData] TO [noContactInfo]
GRANT SELECT ON [dbo].[customerData] TO [noContactInfo]

GRANT ALTER ON [dbo].[physicalAddress] TO [noContactInfo]
GRANT INSERT ON [dbo].[physicalAddress] TO [noContactInfo]
GRANT SELECT ON [dbo].[physicalAddress] TO [noContactInfo]

GRANT ALTER ON [dbo].[fieldTechData] TO [noContactInfo]
GRANT INSERT ON [dbo].[fieldTechData] TO [noContactInfo]
GRANT SELECT ON [dbo].[fieldTechData] TO [noContactInfo]

GRANT ALTER ON [dbo].[workOrder] TO [noContactInfo]
GRANT INSERT ON [dbo].[workOrder] TO [noContactInfo]
GRANT SELECT ON [dbo].[workOrder] TO [noContactInfo]

GRANT ALTER ON [dbo].[serviceLocation] TO [noContactInfo]
GRANT INSERT ON [dbo].[serviceLocation] TO [noContactInfo]
GRANT SELECT ON [dbo].[serviceLocation] TO [noContactInfo]