/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Student: Roger Pierson
Purpose: Create Tables in Work Order Processing Database
Rev: 2021-03-20
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
BEGIN TRANSACTION
SET XACT_ABORT ON -- Abort entire transaction on any error

-- ====  Create physical Address Table ==== 
CREATE TABLE physicalAddress
	(
	[mailID] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[locationType] VARCHAR(255),
	[houseNumber] VARCHAR(255),
	[streetName] VARCHAR(255),
	[cityName] VARCHAR(255),
	stateAbbreviation VARCHAR(255),
	zipcode VARCHAR(255)
	);
GO

-- ====  Create People Table ==== 
CREATE TABLE people
	(
	[personID] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[personCategory] VARCHAR(255),
	[firstName] VARCHAR(255),
	[lastName] VARCHAR(255)
	);
GO

-- ====  Create Phone Number Table ==== 
CREATE TABLE phoneNumber
	(
	[phoneID] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[personID] TINYINT FOREIGN KEY REFERENCES people(personID),
	[phoneDescription] VARCHAR(255),
	[phoneNumber] VARCHAR(255)
	);
GO

-- ====  Create Customer Data Table ==== 
CREATE TABLE customerData
	(
	[customerID] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[companyName] VARCHAR(255),
	[hqPhysicalAddress] TINYINT FOREIGN KEY REFERENCES physicalAddress(mailID),
	[hqContactName] TINYINT FOREIGN KEY REFERENCES people(personID)
	);
GO


-- ====  Create Service Locations Table ==== 
CREATE TABLE serviceLocation
	(
	[siteID] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[siteDescription] VARCHAR(255),
	[customerID] TINYINT FOREIGN KEY REFERENCES customerData(customerID),
	[sitePhysicalAddress] TINYINT FOREIGN KEY REFERENCES physicalAddress(mailID),
	[siteContactName] TINYINT FOREIGN KEY REFERENCES people(personID),
	[siteArrivalInstructions] VARCHAR(255)
	);
GO

-- ====  Create Email Address Table ==== 
CREATE TABLE emailAddress
	(
	[emailID] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[ownerID] TINYINT FOREIGN KEY REFERENCES people(personID),
	[emailDescription] VARCHAR(255),
	[emailAddress] VARCHAR(255)
	);
GO

-- ====  Create Field Tech Data Table ==== 
CREATE TABLE fieldTechData
	(
	[TechID] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[TechName] TINYINT FOREIGN KEY REFERENCES people(personID),
	[laborRate] VARCHAR(255)
	);
GO

-- ====  Create Work Order Table ==== 
CREATE TABLE workOrder
	(
	[workOrderNumber] TINYINT NOT NULL IDENTITY PRIMARY KEY,
	[siteID] TINYINT FOREIGN KEY REFERENCES serviceLocation(siteID),
	[techID] TINYINT FOREIGN KEY REFERENCES fieldTechData([TechID]),
	[customerComplaint] VARCHAR(255),
	[estimatedHours] VARCHAR(255),
	[estimatedCost] VARCHAR(255),
	[techDescriptionOfWork] VARCHAR(255),
	[jobStartTime] VARCHAR(255),
	[jobStopTime] VARCHAR(255),
	[actualCost] VARCHAR(255)
	);
GO
COMMIT;

