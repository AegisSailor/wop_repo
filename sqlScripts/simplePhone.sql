SELECT phoneNumber.phoneID, phoneNumber.personID, people.firstName, people.lastName, phoneNumber.phoneDescription, phoneNumber.phoneNumber
FROM   phoneNumber INNER JOIN
             people ON phoneNumber.personID = people.personID