SELECT emailAddress.emailID, emailAddress.ownerID, people.firstName, people.lastName, emailAddress.emailDescription, emailAddress.emailAddress
FROM   emailAddress INNER JOIN
             people ON emailAddress.ownerID = people.personID