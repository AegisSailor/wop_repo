<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Examine Business Logic Layer</title>
	</head>
	<body>
		<h3>The text below is the contents of dbFunctions.php, which contains the code for the logic layer</h3>
		<br>
		<hr>
		<div id="logicLayer"></div>
		<script>
			var w = window.open('dbFunctions.php'); //Required full file path.
			document.getElementById("logicLayer").innerHTML = w
		</script>
	</body>
</html>