<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Database Connection Info</title>
	</head>
	<body id="main_body" >
		<div id="form_container">	
			<form id="dbInfo" method="post" action="/dbTest.php">
				<div>
					<h2>Database Connection Info</h2>
					<p>Enter connection information for the database, or accept .</p>
				</div>						
						<label class="description" for="svrName">Database Server Name </label>
							<div>
								<input id="svrName" name="svrName" class="element text medium" type="text" maxlength="255" value=""/> 
							</div><p class="guidelines" id="guide_1"></p> 
						
						<label class="description" for="dbName">Database Name </label>
							<div>
								<input id="dbName" name="dbName" class="element text medium" type="text" maxlength="255" value=""/> 
							</div><p class="guidelines" id="guide_2"></p> 

						<label class="description" for="usrName">User Name </label>
							<div>
<!--								<input id="usrName" name="usrName" class="element text medium" type="text" maxlength="255" value=""/> -->
								<select id="usrName" name="usrName">
									<option value="wopAdmin">Admin</option>
									<option value="lowUser">Limited User</option>
								</select>
							
							</div><p class="guidelines" id="guide_3"></p> 

						<label class="description" for="pwd">Password </label>
							<div>
								<input id="pwd" name="pwd" class="element text medium" type="text" maxlength="255" value=""/> 
							</div><p class="guidelines" id="guide_4"></p> 


						<input type="hidden" name="form_id" value="Connection Info" />
						
						<input id="saveForm" class="button_text" type="submit" name="submit" value="Test Conection" />
			</form>	
			
			<script>
				// FILL IN FORM DEFAULTS
				document.getElementById('svrName').value = 'wop-server.database.windows.net';
				document.getElementById('dbName').value = 'wopDB';
				document.getElementById('usrName').value = 'wopAdmin';
				document.getElementById('pwd').value = '1Purdue1';
			</script>
		
		</div>
	</body>
</html>
