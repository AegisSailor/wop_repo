<?php
	session_start();
	include './dbFunctions.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>View Database </title>
	</head>
	<body>
		<br><br>
		<h3><a href="/index.html">Go Home</a></h3>
		<br><br>
		<b>Note:</b> User lowUser belongs to the noContactInfo database role.  This role does not have SELECT permissions on email and phone tables, therefore when connecting to the database with this ueser the tables below that include this data will be empty becquase the query returns nothing. <br>
		<?php
			// GET DB CONNECTION
			$db = makeWopConnection();
			$usr = $_SESSION['dbInfo']['usrName'];
			echo "USER: $usr";
			echo "<hr>";
			
			// GET PEOPLE
			$query = "SELECT [personID],[personCategory],[firstName],[lastName]	FROM [dbo].[people]"; 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT PEOPLE
			echo "<h3><u>PEOPLE</u></h3>";
			echo "<table border='1'>";
				echo "<tr>";
					echo "<th>personID</th>";
					echo "<th>personCategory</th>";
					echo "<th>firstName</th>";
					echo "<th>lastName</th>";
				echo "</tr>";
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
					{  
						echo "<tr>";
							echo "<td>".$row[0]."</td>";  
							echo "<td>".$row[1]."</td>";  
							echo "<td>".$row[2]."</td>";  
							echo "<td>".$row[3]."</td>";
						echo "</tr>";
					}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			//sqlsrv_free_stmt($stmt);  
					
			$query = file_get_contents("sqlScripts/simplePhone.sql"); 
			$stmt = sqlsrv_query($db, $query);

			// PRINT PHONE
			echo "<h3><u>PHONE</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
				echo "<th>phoneID</th>";
				echo "<th>personID</th>";
				echo "<th>firstName</th>";
				echo "<th>lastName</th>";
				echo "<th>phoneDescription</th>";
				echo "<th>phoneNumber</th>";
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "<td>".$row[3]."</td>";
					echo "<td>".$row[4]."</td>";
					echo "<td>".$row[5]."</td>";
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			//sqlsrv_free_stmt($stmt);  
			
			// GET EMAIL
			$query = file_get_contents("sqlScripts/simpleEmail.sql"); 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT EMAIL
			echo "<h3><u>EMAIL</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>emailID</th>";
			echo "<th>owerID</th>";
			echo "<th>firstName</th>";
			echo "<th>lastName</th>";
			echo "<th>emailDescription</th>";
			echo "<th>emailAddress</th>";
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "<td>".$row[3]."</td>";
					echo "<td>".$row[4]."</td>";
					echo "<td>".$row[5]."</td>";
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			//sqlsrv_free_stmt($stmt);  
			
			// GET PHYSICAL ADDRESS
			$query = "SELECT [mailID],[locationType],[houseNumber],[streetName],[cityName],[stateAbbreviation],[zipcode] FROM [dbo].[physicalAddress]"; 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT PHYSICAL ADDRESS
			echo "<h3><u>PHYSICAL ADDRESS</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>mailID</th>";
			echo "<th>locationType</th>";
			echo "<th>houseNumber</th>";
			echo "<th>streetName</th>";
			echo "<th>cityName</th>";
			echo "<th>stateAbbreviation</th>";
			echo "<th>zipcode</th>";
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "<td>".$row[3]."</td>";
					echo "<td>".$row[4]."</td>";
					echo "<td>".$row[5]."</td>";
					echo "<td>".$row[6]."</td>";
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			//sqlsrv_free_stmt($stmt);  

			// GET CUSTOMER DATA
			$query = "SELECT [customerID],[companyName],[hqPhysicalAddress],[hqContactName]	FROM [dbo].[customerData]"; 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT CUSTOMER DATA
			echo "<h3><u>CUSTOMER DATA</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>customerID</th>";
			echo "<th>companyName</th>";
			echo "<th>hqPhysicalAddress</th>";
			echo "<th>hqContactName</th>";
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "<td>".$row[3]."</td>";
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			//sqlsrv_free_stmt($stmt);  
			
			// GET SERVICE LOCATION
			$query = file_get_contents("sqlScripts/simpleServiceLocation.sql"); 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT SERVICE LOCATION
			echo "<h3><u>SERVICE LOCATION</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>siteID</th>";
			echo "<th>customerID</th>";
			echo "<th>customerName</th>";
			echo "<th>siteDescription</th>";
			echo "<th>sitePhysicalAddress</th>";
			echo "<th>houseNumber</th>";
			echo "<th>streetName</th>";
			echo "<th>cityName</th>";
			echo "<th>stateAbbreviation</th>";
			echo "<th>zipCode</th>";
			echo "<th>siteContactName</th>";
			echo "<th>POCname</th>";
			echo "<th>phoneDescription</th>";
			echo "<th>phoneNumber</th>";
			echo "<th>emailDescription</th>";
			echo "<th>emailAddress</th>";
			echo "<th>siteArrivalInstructions</th>";
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "<td>".$row[3]."</td>";
					echo "<td>".$row[4]."</td>";
					echo "<td>".$row[5]."</td>";
					echo "<td>".$row[6]."</td>";  
					echo "<td>".$row[7]."</td>";  
					echo "<td>".$row[8]."</td>";  
					echo "<td>".$row[9]."</td>";
					echo "<td>".$row[10]."</td>";
					echo "<td>".$row[11]."</td>";
					echo "<td>".$row[12]."</td>";  
					echo "<td>".$row[13]."</td>";  
					echo "<td>".$row[14]."</td>";  
					echo "<td>".$row[15]."</td>";
					echo "<td>".$row[16]."</td>";
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			//sqlsrv_free_stmt($stmt);  
			
			// GET FIELD TECH DATA
			$query = "SELECT [TechID],[TechName],[laborRate]	FROM [dbo].[fieldTechData]"; 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT FIELD TECH DATA
			echo "<h3><u>FIELD TECH DATA</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>TechID</th>";
			echo "<th>TechName</th>";
			echo "<th>TechRate</th>";
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			//sqlsrv_free_stmt($stmt);  
			
			// GET WORK ORDER
			$query = "SELECT [workOrderNumber],[siteID],[techID],[customerComplaint],[estimatedHours],[estimatedCost]
			,[techDescriptionOfWork],[jobStartTime],[jobStopTime],[ActualCost] FROM [dbo].[workOrder]"; 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT WORK ORDER
			echo "<h3><u>WORK ORDER</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>workOrderNumber</th>";
			echo "<th>siteID</th>";
			echo "<th>techID</th>";
			echo "<th>customerComplaint</th>";
			echo "<th>estimatedHours</th>";
			echo "<th>estimatedCost</th>";
			echo "<th>techDescriptionOfWork</th>";
			echo "<th>jobStartTime</th>";
			echo "<th>jobStopTime</th>";
			echo "<th>ActualCost</th>";			
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "<td>".$row[3]."</td>";
					echo "<td>".$row[4]."</td>";
					echo "<td>".$row[5]."</td>";
					echo "<td>".$row[6]."</td>";
					echo "<td>".$row[7]."</td>";
					echo "<td>".$row[8]."</td>";
					echo "<td>".$row[9]."</td>";
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			// FREE STATMENT
			sqlsrv_free_stmt($stmt);  			
			
			
			
			
			
			// CLOSE CONNECTION
			sqlsrv_close($db);

		?>
	</body>
</html>