<?php
	session_start();
	include './dbFunctions.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Show Work Order </title>
	</head>
	<body>
		<br><br>
		<h3><a href="/index.html">Go Home</a></h3>
		<br><br>
		<?php
			// GET DB CONNECTION
			$db = makeWopConnection();
			$woNum = $_POST['woNum'];
			$query = "SELECT [workOrderNumber],[siteID],[techID],[customerComplaint],[estimatedHours],[estimatedCost]
			,[techDescriptionOfWork],[jobStartTime],[jobStopTime],[ActualCost] FROM [dbo].[workOrder] WHERE workOrderNumber=$woNum"; 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT WORK ORDER
			echo "<h3><u>WORK ORDER</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>workOrderNumber</th>";
			echo "<th>siteID</th>";
			echo "<th>techID</th>";
			echo "<th>customerComplaint</th>";
			echo "<th>estimatedHours</th>";
			echo "<th>estimatedCost</th>";
			echo "<th>techDescriptionOfWork</th>";
			echo "<th>jobStartTime</th>";
			echo "<th>jobStopTime</th>";
			echo "<th>ActualCost</th>";			
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[1]."</td>";  
					echo "<td>".$row[2]."</td>";  
					echo "<td>".$row[3]."</td>";
					echo "<td>".$row[4]."</td>";
					echo "<td>".$row[5]."</td>";
					echo "<td>".$row[6]."</td>";
					echo "<td>".$row[7]."</td>";
					echo "<td>".$row[8]."</td>";
					echo "<td>".$row[9]."</td>";
					echo "</tr>";
				}  
			echo "</table><br><br>";
		?>
	</body>
</html>