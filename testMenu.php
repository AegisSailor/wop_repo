<?php
    session_start();
?>

<html>
 <head>
  <title>Test Menu</title>
 </head>
 <body>
    <br><br>
    <h3><a href="/index.html">Go Home</a></h3>
    <h2>Menu Options</h2>
    <ol>
      <li><a href="/drop.php">Drop the Database</a></li>
      <li><a href="/create.php">Create the Database</a></li>
      <li><a href="/populate.php">Populate the Database</a></li>
      <li><a href="/view.php">View the Database</a></li>

    </ol>
    <hr />

  </body>
  
</html>