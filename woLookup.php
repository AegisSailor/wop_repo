<?php
	session_start();
	include './dbFunctions.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Lookup Work Order </title>
	</head>
	<body>
		<br><br>
		<h3><a href="/index.html">Go Home</a></h3>
		<br><br>
		<?php
			// GET DB CONNECTION
			$db = makeWopConnection();
			
			$query = "SELECT [workOrderNumber],[siteID] FROM [dbo].[workOrder]"; 
			$stmt = sqlsrv_query($db, $query);
			
			// PRINT WORK ORDER
			echo "<h3><u>WORK ORDER</u></h3>";
			echo "<table border='1'>";
			echo "<tr>";
			echo "<th>workOrderNumber</th>";
			echo "<th>customerComplaint</th>";
			echo "</tr>";
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  
				{  
					echo "<tr>";
					echo "<td>".$row[0]."</td>";  
					echo "<td>".$row[3]."</td>";  
					echo "</tr>";
				}  
			echo "</table><br><br>";
			
			
			// FREE STATMENT
			sqlsrv_free_stmt($stmt);  			

			// CLOSE CONNECTION
			sqlsrv_close($db);
		?>
		<form id="woLookup" method="post" action="/showWorkOrder.php">
			<div>
				<p>Enter the work order number to view.</p>
			</div>						
			<label class="description" for="woNum">Work Order Number </label>
			<div>
				<input id="woNum" name="woNum" class="element text medium" type="text" maxlength="255" value=""/> 
			</div>			
			
			<input type="hidden" name="form_id" value="Work Order Lookup" />
			
			<input id="saveForm" class="button_text" type="submit" name="submit" value="Show Work Order" />
		</form>	

	</body>
</html>