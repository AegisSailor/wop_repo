<?php
    session_start();
    include './dbFunctions.php';
?>

<html>
 <head>
  <title>Database Connection Test</title>
 </head>
 <body>
    <br><br>
        <?php 
            // SAVE POST DATA TO PHP SESSION VARIABLE
            // Can be accessed later using: $_SESSION['post-data']['variableName']
            $_SESSION['dbInfo'] = $_POST;
        ?>
    
    <h2>Connection Test Results</h2>
    <?php
        
        if(is_resource(makeWopConnection()))
            {
                echo "<h3>Connection Successful!</h3><br><br>";
                echo "<h3><a href='/index.html'>Go Home</a></h3>";
            }
        else
            {
                echo "<h3>COULD NOT CONNECT! Please <a href='/dbConnect.php'>check connection information</a></h3><br>";
                die("Unable to proceed no DB connection");
                echo "<br><br>";
                echo "<h4> ERROR INFORMATION </h4>";
                die( print_r( sqlsrv_errors(), true));  
            }

    ?>  
    
</body>
    
</html>



