<?php
	//-----------------------------------------------  
	// Purpose: This file contains functions for database operations
	// Author: Roger Pierson
	// Revision: 28MAR21
	//-----------------------------------------------  
	
	//------------------------------ 
	// RETURN CONNECTION TO WORK ORDER DATABASE
	//------------------------------ 
	
	function makeWopConnection() {
		//Returns a connection object if successful.  
		
		// SET CONNECTION PARAMETERS
		if (isset($_SESSION)) {
			//User submitted the connection info form
			$serverName = $_SESSION['dbInfo']['svrName'];
			$databaseName = $_SESSION['dbInfo']['dbName'];
			$userName = $_SESSION['dbInfo']['usrName'];
			$userPassword = $_SESSION['dbInfo']['pwd'];
		}
		else 
			{
			// User did NOT submit conenction data
			echo "<h2>Please <a href='/dbConnect.php'>Connect to database</a> first!</h2><br>";
			die("Unable to proceed no DB connection");
		}
		
		// MAKE THE CONNECTION
		$connectionInfo = array("UID" => $userName, "pwd" => $userPassword, "Database" => $databaseName);
		$conn = sqlsrv_connect( $serverName, $connectionInfo);  
		
		// TEST THE CONNECTION
		if( $conn )  
			{  
				return $conn;
			}  
		else  
			{  
				echo "<h2>COULD NOT CONNECT! Please <a href='/dbConnect.php'>check connection information</a></h2><br>";
				die("Unable to proceed no DB connection");
				echo "<br><br>";
				echo "<h4> ERROR INFORMATION </h4>";
				die( print_r( sqlsrv_errors(), true));  
			}  
	}
	
	//------------------------------ 
	// RETURN CONNECTION TO MASTER DATABASE
	//------------------------------ 
	function makeMasterConnection() {
		//Returns a connection object if successful.  
		
		// SET CONNECTION PARAMETERS
		if (isset($_SESSION)) {
			//User submitted the connection info form
			$serverName = $_SESSION['dbInfo']['svrName'];
			$databaseName = "master";
			$userName = $_SESSION['dbInfo']['usrName'];
			$userPassword = $_SESSION['dbInfo']['pwd'];
		}
		else 
			{
				// User did NOT submit conenction data
				echo "<h2>Please <a href='/dbConnect.php'>Connect to database</a> first!</h2><br>";
				die("Unable to proceed no DB connection");
			}
		
		// MAKE THE CONNECTION
		$connectionInfo = array("UID" => $userName, "pwd" => $userPassword, "Database" => $databaseName);
		$conn = sqlsrv_connect( $serverName, $connectionInfo);  
		
		// TEST THE CONNECTION
		if( $conn )  
			{  
			return $conn;
			}  
		else  
			{  
				echo "<h2>COULD NOT CONNECT! Please <a href='/dbConnect.php'>check connection information</a></h2><br>";
				die("Unable to proceed no DB connection");
				echo "<br><br>";
				echo "<h4> ERROR INFORMATION </h4>";
				die( print_r( sqlsrv_errors(), true));  
			}  
	}
	
	// DROP THE WORK ORDER DATABASE
	function dropWopDb() {
		$db = makeMasterConnection();
		$query = file_get_contents("sqlScripts/drop.sql"); 
		$stmt = sqlsrv_query($db, $query);
		if ($stmt) {  
			
			echo "Database dropped!.\n";  
		} else {  
			echo "Drop failed!\n"; 
			die(print_r(sqlsrv_errors(), true));  
		}  
		
		// FREE RESOURCES
		sqlsrv_free_stmt($stmt);  
		sqlsrv_close($db);
	}
	
	// CREATE THE WORK ORDER DATABASE
	function createWopDb() {
		$db = makeMasterConnection();
		$query = file_get_contents("sqlScripts/create.sql"); 
		$stmt = sqlsrv_query($db, $query);
		if ($stmt) {  
			echo "Database created.\n";  
		} else {  
			echo "Create failed.\n"; 
			die(print_r(sqlsrv_errors(), true));  
		}  
		
		// FREE RESOURCES
		sqlsrv_free_stmt($stmt);  
		sqlsrv_close($db);
	}

	// POPULATE THE WORK ORDER DATABASE
	function populateWopDb() {
		$db = makeWopConnection();
		$query = file_get_contents("sqlScripts/populate.sql"); 
		$stmt = sqlsrv_query($db, $query);
		if ($stmt) {  
			
			echo "Database populated!.\n";  
		} else {  
			echo "Populate failed!.\n"; 
			die(print_r(sqlsrv_errors(), true));  
		}  
		
		// FREE RESOURCES
		sqlsrv_free_stmt($stmt);  
		sqlsrv_close($db);
	}
	
?>

